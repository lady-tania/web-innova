import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  today: any;

  constructor() { }

  ngOnInit(): void {
    this.fechaActual();
  }

  fechaActual() {
    const now = new Date();
    const fechastring = now.toISOString();
    this.today = fechastring.split('T')[0];
    console.log(this.today);
  }

}

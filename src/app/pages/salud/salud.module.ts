import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaludRoutingModule } from './salud-routing.module';
import { SaludComponent } from './salud.component';


@NgModule({
  declarations: [
    SaludComponent
  ],
  imports: [
    CommonModule,
    SaludRoutingModule
  ]
})
export class SaludModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReservaFichaComponent } from './reserva-ficha.component';

const routes: Routes = [{ path: '', component: ReservaFichaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservaFichaRoutingModule { }

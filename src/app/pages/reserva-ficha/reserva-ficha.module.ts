import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservaFichaRoutingModule } from './reserva-ficha-routing.module';
import { ReservaFichaComponent } from './reserva-ficha.component';


@NgModule({
  declarations: [
    ReservaFichaComponent
  ],
  imports: [
    CommonModule,
    ReservaFichaRoutingModule
  ]
})
export class ReservaFichaModule { }

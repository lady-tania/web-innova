import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservaFichaComponent } from './reserva-ficha.component';

describe('ReservaFichaComponent', () => {
  let component: ReservaFichaComponent;
  let fixture: ComponentFixture<ReservaFichaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservaFichaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservaFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

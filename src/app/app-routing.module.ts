import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
  { path: 'salud', loadChildren: () => import('./pages/salud/salud.module').then(m => m.SaludModule) },
  { path: 'reserva-ficha', loadChildren: () => import('./pages/reserva-ficha/reserva-ficha.module').then(m => m.ReservaFichaModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
